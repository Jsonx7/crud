let closemodal = () => {
    $('#exampleModal').modal('hide');
}
let listClient = document.getElementById('tbody');

//CRUD
let getLocalStorage = () => {
    if (JSON.parse(localStorage.getItem("db_cliente")) == null) {
        return [];
    } else {
        return JSON.parse(localStorage.getItem("db_cliente"))

    }
}
let setLocalStorage = (dbCliente) => localStorage.setItem("db_cliente", JSON.stringify(dbCliente));
//borrar
let deleteClient = (index) => {
        let dbCliente = readClient();
        dbCliente.splice(index, 1);
        setLocalStorage(dbCliente);
    }
    //actualizar
let updateClient = (index, client) => {
        let dbCliente = readClient();
        dbCliente[index] = client;
        setLocalStorage(dbCliente);
    }
    //leer
let readClient = () => getLocalStorage();
//crear
let createClient = (client) => {
        let dbCliente = getLocalStorage();
        dbCliente.push(client);
        setLocalStorage(dbCliente);
    }
    //validar inputs
let isValidFields = () => {
        return document.getElementById("form").reportValidity();
    }
    //*************interaccion con el formulario********************

//limpiar campos
let clearfields = () => {
    let fields = document.querySelectorAll('.form-control');
    fields.forEach(field => field.value = "");
}

//boton guardar
let saveclient = () => {
    if (isValidFields()) {
        let client = {
            id: document.getElementById('id').value,
            nombre: document.getElementById('nombre').value,
            nit: document.getElementById('nit').value,
            fecha: document.getElementById('fecha').value,
            direccion: document.getElementById('direccion').value

        }
        let index = document.getElementById("nombre").dataset.index;
        if (index == "new") {
            createClient(client);
            clearfields();
            updateTable();
            closemodal();
        } else {
            updateClient(index, client);
            updateTable();
            closemodal();
        }


    }
}

let updateTable = () => {
    listClient.innerHTML = "";
    let dbClient = readClient();
    for (i = 0; i < dbClient.length; i++) {
        listClient.innerHTML += `
        <tr>
                    <td>${dbClient[i].id}</td>
                    <td>${dbClient[i].nombre}</td>
                    <td>${dbClient[i].nit}</td>
                    <td>${dbClient[i].fecha}</td>
                    <td>${dbClient[i].direccion}</td>
                    <td><button type="button" class="btn btn-warning" id="edit-${i}">edit</button>
                        <button type="button" class="btn btn-danger" id="delete-${i}">delete</button>
                    </td>
                </tr>
        `
    }
}
let fillFields = (client) => {
    document.getElementById('id').value = client.id;
    document.getElementById('nombre').value = client.nombre;
    document.getElementById('nit').value = client.nit;
    document.getElementById('fecha').value = client.fecha;
    document.getElementById('direccion').value = client.direccion;
    document.getElementById('nombre').dataset.index = client.index;
}
let editClient = (index) => {
    let client = readClient()[index];
    client.index = index;
    fillFields(client);
    $('#exampleModal').modal('show');

}
let editdelete = (event) => {
        if (event.target.type == 'button') {
            let [action, index] = event.target.id.split('-');
            if (action == "edit") {
                editClient(index);
            } else {
                deleteClient(index);
                updateTable();
            }
        }


    }
    //cuando se cierra el modal limpiar inputs
$('#exampleModal').on('hidden.bs.modal', function(event) {
    clearfields();
})
updateTable();
//eventos
document.getElementById("savebtn")
    .addEventListener('click', saveclient);
document.querySelector('#tbody')
    .addEventListener('click', editdelete);